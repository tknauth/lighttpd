#!/bin/bash

# set -eux
set -e
set -x

git clone https://bitbucket.org/tknauth/lighttpd lighttpd-sgx

CFLAGS="-fPIC -DLIGHTTPD_STATIC -g -ggdb"

pushd lighttpd-sgx
./autogen.sh
CC=$MUSLCC ./configure --without-zlib --without-bzip2 --without-pcre --disable-ipv6 --disable-lfs --enable-static --disable-shared CFLAGS="$CFLAGS" LDFLAGS="-lcrypt"
make CC=$MUSLCC
cp src/lemon .
make CC=$SGXCC
cp lemon src/
make CC=$SGXCC

pushd src

$SGXCC "$CFLAGS" -Wall -W -Wshadow -pedantic -std=gnu99 -shared -o lighttpd.so server.o response.o connections.o network.o configfile.o configparser.o request.o proc_open.o base64.o buffer.o log.o mod_access.o mod_accesslog.o mod_alias.o mod_auth.o mod_cgi.o mod_compress.o mod_dirlisting.o mod_evhost.o mod_expire.o mod_extforward.o mod_fastcgi.o mod_flv_streaming.o mod_indexfile.o mod_proxy.o mod_rrdtool.o mod_scgi.o mod_secdownload.o mod_setenv.o mod_simple_vhost.o mod_staticfile.o mod_status.o mod_userdir.o mod_usertrack.o mod_webdav.o http_auth.o keyvalue.o chunk.o http_chunk.o stream.o fdevent.o stat_cache.o plugin.o joblist.o etag.o array.o data_string.o data_count.o data_array.o data_integer.o md5.o data_fastcgi.o fdevent_select.o fdevent_libev.o fdevent_poll.o fdevent_linux_sysepoll.o fdevent_solaris_devpoll.o fdevent_solaris_port.o fdevent_freebsd_kqueue.o data_config.o inet_ntop_cache.o crc32.o connections-glue.o configfile-glue.o http-header-glue.o network_write.o network_linux_sendfile.o network_write_mmap.o network_write_no_mmap.o network_freebsd_sendfile.o network_writev.o network_solaris_sendfilev.o network_openssl.o splaytree.o status_counter.o safe_memclear.o network_darwin_sendfile.o -Wl,--export-dynamic

popd

popd
